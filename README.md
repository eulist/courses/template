# Template for course metadata

In the `metadata.yml` file you can find placeholders for course metadata which can be copied and then adapted to your metadata.

## Video tutorial
![Video tutorial](tutorial.mp4)